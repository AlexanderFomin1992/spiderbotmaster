package com.afomin.spiderbotmaster;

import android.content.Context;
import android.content.res.ColorStateList;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Map;
import java.util.concurrent.Callable;

import androidx.appcompat.app.AppCompatActivity;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    // Socket instance for communication with target
    DatagramSocket datagramSocket;
    // callable class for calculating rotation angles - pitch, roll, azimuth
    private RotationAnglesCalculator rotationAnglesCalculator;
    // Observable class for rotation angles
    private Observable sensorDataSource;
    // Disposable class for disconnecting Observable
    private Disposable disposable;

    static long lastUpdateTimeMsec = 0;

    private static Display mDisplay;

    private SensorManager mSensorManager;
    private Sensor mSensorAccelerometer;
    private Sensor mSensorMagnetometer;

    // TextViews to display current sensor values.
    private TextView mTextSensorAzimuth;
    private TextView mTextSensorPitch;
    private TextView mTextSensorRoll;

    // LinearLayout with debug print
    private LinearLayout mLinearLayoutDebugPrint;

    // Data values to store accelerometer and magnetometer values
    private static float [] mAccelerometerData = new float[3];
    private static float [] mMagnetometerData = new float[3];

    // constants for state
    private static boolean masterSendDataFlag;

    private ImageView mSpotTop;
    private ImageView mSpotBottom;
    private ImageView mSpotLeft;
    private ImageView mSpotRight;

    // String contants, described current state
    private static final String FORWARD = "FORWARD";
    private static final String REVERSE = "REVERSE";
    private static final String RIGHT = "RIGHT";
    private static final String LEFT = "LEFT";
    private static final String NEUTRAL = "NEUTRAL";

    // index inside rotationAngles
    private static final int AZIMUTH = 0;
    private static final int PITCH = 1;
    private static final int ROLL = 2;

    private final String TAG = "SpiderBotMasterDebug";

    @Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
        initializeStartStopButton();
        initializeSettingsButton();
    }

    /**
     * Do all stuff for initialize User Interface
     */
    private void initializeUI() {
        mTextSensorAzimuth = (TextView) findViewById(R.id.value_azimuth);
        mTextSensorPitch = (TextView) findViewById(R.id.value_pitch);
        mTextSensorRoll = (TextView) findViewById(R.id.value_roll);
        mTextSensorAzimuth.setText("");
        mTextSensorPitch.setText("");
        mTextSensorRoll.setText("");

        mSpotLeft = (ImageView) findViewById(R.id.spot_left);
        mSpotRight = (ImageView) findViewById(R.id.spot_right);
        mSpotTop = (ImageView) findViewById(R.id.spot_top);
        mSpotBottom = (ImageView) findViewById(R.id.spot_bottom);

        mLinearLayoutDebugPrint = (LinearLayout) findViewById(R.id.grop_layout_debug_output);
        mLinearLayoutDebugPrint.setVisibility(View.VISIBLE);

        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mDisplay = windowManager.getDefaultDisplay();

        // Get accelerometer and magnetometer sensors from the sensor manager.
        // The getDefaultSensor() method returns null if the sensor
        // is not available on the device.
        mSensorManager = (SensorManager) getSystemService(
                Context.SENSOR_SERVICE);
        mSensorAccelerometer = mSensorManager.getDefaultSensor(
                Sensor.TYPE_ACCELEROMETER);
        mSensorMagnetometer = mSensorManager.getDefaultSensor(
                Sensor.TYPE_MAGNETIC_FIELD);

        rotationAnglesCalculator = new RotationAnglesCalculator();
    }

    /**
     * Do all stuff for initialize floating action button StartStop
     */
    private void initializeStartStopButton() {
        final FloatingActionButton fabStartStop = findViewById(R.id.fab_start_stop);
        fabStartStop.setRippleColor(getResources().getColor(R.color.yellow));
        fabStartStop.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.green)));
        fabStartStop.setScaleType(ImageView.ScaleType.CENTER);
        fabStartStop.setImageDrawable(getResources().getDrawable(R.drawable.ic_directions_run_black_24dp));
        fabStartStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                masterSendDataFlag = !masterSendDataFlag;
                if (masterSendDataFlag == true) {
                    fabStartStop.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_circle_outline_black_24dp));
                    fabStartStop.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.red)));
                } else {
                    fabStartStop.setImageDrawable(getResources().getDrawable(R.drawable.ic_directions_run_black_24dp));
                    fabStartStop.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.green)));
                    mSpotTop.setBackgroundColor(getResources().getColor(R.color.black));
                    mSpotBottom.setBackgroundColor(getResources().getColor(R.color.black));
                    mSpotRight.setBackgroundColor(getResources().getColor(R.color.black));
                    mSpotLeft.setBackgroundColor(getResources().getColor(R.color.black));
                }
            }
        });
    }

    /**
     * Do all stuff for initialize floating action button Settings
     */
    private void initializeSettingsButton() {
        FloatingActionButton fab_settings = findViewById(R.id.fab_settings);
        fab_settings.setRippleColor(getResources().getColor(R.color.yellow));
        fab_settings.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.gray)));
        fab_settings.setImageDrawable(getResources().getDrawable(R.drawable.ic_build_black_24dp));
        fab_settings.setScaleType(ImageView.ScaleType.CENTER);
        fab_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SettingsDialogFragment settingsDialogFragment = new SettingsDialogFragment();
                settingsDialogFragment.show(getSupportFragmentManager(), "settingsDialogFragment");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mSensorAccelerometer != null) {
            mSensorManager.registerListener(this, mSensorAccelerometer,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
        if (mSensorMagnetometer != null) {
            mSensorManager.registerListener(this, mSensorMagnetometer,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
        masterSendDataFlag = false;
        Settings.getAllParametersFromSharedPreferences(this);
        startCommunication();
    }

    private void startCommunication() {
        connectUDPSocket();
        initializeSensorProcessing();
    }

    private void stopCommunication() {
        deinitializeSensorProcessing();
        disconnectUDPSocket();
    }

    public void restartCommunication() {
        stopCommunication();
        startCommunication();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
        masterSendDataFlag = false;
        stopCommunication();
    }



    /**
     * create UDP socket (first of all call disconnectUDPSocket and
     * safely disconnect from previous instance)
     */
    private void connectUDPSocket() {
        try {
            disconnectUDPSocket();
            datagramSocket = new DatagramSocket();
        } catch (SocketException e) {

        } catch (SecurityException e) {

        }
    }

    /**
     * Disconnect from UDP socket (if UDP socket exist and is connected )
     */
    private void disconnectUDPSocket() {
        if (datagramSocket != null) {
            if (datagramSocket.isConnected()) {
                datagramSocket.close();
            }
        }
    }

    /**
     * Full rxJava process.
     * Make:
     * 1. Get data from Callable rotationAnglesCalculator - angles in radians
     * 2. Filter by mode (computation thread)
     * 3. Filter by time (computation thread)
     * 4. Map angles by small drift, add offset to pitch(computation thread)
     * 5. Update spot color according to anglesToState rule - implemented as filter, always return true
     * 6. Send state string to udp socket (io thread)
     *
     */
    private void initializeSensorProcessing() {
        final Map<String, String> mapSharedPreferences  = Settings.getAllParametersFromSharedPreferences(this);

        final float neutral_position_maximum_abs_value_radians = (float) Math.toRadians(
                Double.valueOf(mapSharedPreferences.get(getResources().getString(
                        R.string.shared_preferences_neutral_position_maximum_abs_value_key))));
        final float pitch_maximum_abs_value_radians = (float) Math.toRadians(
                Double.valueOf(mapSharedPreferences.get(getResources().getString(
                        R.string.shared_preferences_pitch_maximum_abs_value_key))));
        final float roll_maximum_abs_value_radians = (float) Math.toRadians(
                Double.valueOf(mapSharedPreferences.get(getResources().getString(
                        R.string.shared_preferences_roll_maximum_abs_value_key))));
        final float pitch_neutral_offset_value_radians = (float) Math.toRadians(
                Double.valueOf(mapSharedPreferences.get(getResources().getString(
                        R.string.shared_preferences_pitch_neutral_offset_key))));

        sensorDataSource = Observable.fromCallable(rotationAnglesCalculator);
        disposable = sensorDataSource
                .filter(new Predicate<float[]>() {
                    @Override
                    public boolean test(float[] rotationAngles) throws Exception {
                        mTextSensorAzimuth.setText(getResources().getString(
                                R.string.value_format, rotationAngles[AZIMUTH]));
                        mTextSensorPitch.setText(getResources().getString(
                                R.string.value_format, rotationAngles[PITCH]));
                        mTextSensorRoll.setText(getResources().getString(
                                R.string.value_format, rotationAngles[ROLL]));
                        return true;
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(Schedulers.computation())
                .filter(new Predicate<float[]>() {
                    @Override
                    public boolean test(float[] rotationAngles) throws Exception {
                        return masterSendDataFlag;
                    }
                })
                .filter(new Predicate<float[]>() {
                    @Override
                    public boolean test(float[] rotationAngles) throws Exception {
                        return timeUpdateFilter(
                                Integer.valueOf(
                                        mapSharedPreferences.get(
                                                getResources().getString(R.string.shared_preferences_update_target_period_key))));
                    }
                })
                .map(new Function() {
                    @Override
                    public Object apply(Object o) throws Exception {
                        float [] rotationAngles = (float []) o;
                        rotationAngles[AZIMUTH] = rotationAngles[AZIMUTH];
                        rotationAngles[PITCH] = (float) (rotationAngles[PITCH] + pitch_neutral_offset_value_radians);
                        rotationAngles[ROLL] = rotationAngles[ROLL];
                        if (Math.abs(rotationAngles[PITCH]) < neutral_position_maximum_abs_value_radians) {
                            rotationAngles[PITCH] = 0;
                        }
                        if (Math.abs(rotationAngles[ROLL]) < neutral_position_maximum_abs_value_radians) {
                            rotationAngles[ROLL] = 0;
                        }
                        return rotationAngles;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .filter(new Predicate<float[]>() {
                    @Override
                    public boolean test(float[] rotationAngles) throws Exception {
                        mSpotTop.setBackgroundColor(getResources().getColor(R.color.black));
                        mSpotBottom.setBackgroundColor(getResources().getColor(R.color.black));
                        mSpotRight.setBackgroundColor(getResources().getColor(R.color.black));
                        mSpotLeft.setBackgroundColor(getResources().getColor(R.color.black));

                        switch (anglesToState(
                                rotationAngles[AZIMUTH],
                                rotationAngles[PITCH],
                                rotationAngles[ROLL],
                                pitch_maximum_abs_value_radians,
                                roll_maximum_abs_value_radians)) {
                            case(FORWARD): {
                                mSpotTop.setBackgroundColor(getResources().getColor(R.color.green));
                                break;
                            } case (REVERSE): {
                                mSpotBottom.setBackgroundColor(getResources().getColor(R.color.green));
                                break;
                            } case (LEFT): {
                                mSpotLeft.setBackgroundColor(getResources().getColor(R.color.green));
                                break;
                            } case (RIGHT): {
                                mSpotRight.setBackgroundColor(getResources().getColor(R.color.green));
                                break;
                            } case (NEUTRAL): {
                                // same as default
                            } default: {
                                // do nothing
                            }
                        }
                        return true;
                    }
                })
                .observeOn(Schedulers.io())
                .repeat()
                .subscribe(new Consumer<float []>() {
                    @Override
                    public void accept(float[] rotationAngles) throws Exception {
                        String state = anglesToState(
                                rotationAngles[AZIMUTH],
                                rotationAngles[PITCH],
                                rotationAngles[ROLL],
                                pitch_maximum_abs_value_radians,
                                roll_maximum_abs_value_radians);
                        if (state.equals(NEUTRAL)) {
                            return;
                        }

                        try {
                            byte[] buf = state.getBytes();
                            DatagramPacket packet = new DatagramPacket(
                                    buf, buf.length,
                                    InetAddress.getByName(
                                            mapSharedPreferences.get(getResources().getString(
                                                    R.string.shared_preferences_ip_address_key))),
                                    Integer.valueOf(
                                            mapSharedPreferences.get(getResources().getString(
                                                    R.string.shared_preferences_target_port_key))));
                            datagramSocket.send(packet);
                        } catch (IOException e) {
                            Log.e(TAG, "IO Error:", e);
                        } catch (Exception e) {
                            Log.d(TAG, "Exception");
                        }
                    }
                },
                new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        // onError
                        System.err.println("observer onError");
                    }
                },
                new Action() {
                    @Override
                    public void run() throws Exception {
                        // onComplete
                        System.err.println("observer complete");
                    }
                },
                new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        // onSubscribe
                        System.err.println("observer onSubscribe");
                    }
                });
    }

    /**
     * Describes general rule for converting rotation angles to state,
     * regardless target (Spider Bot) API.
     * Convert to one of 5 states:
     * FORWARD
     * REVERSE
     * RIGHT
     * LEFT
     * NEUTRAL - nothing to do
     * FORWARD and REVERSE take precedence over RIGHT and LEFT
     *
     * @param azimuth (float) - radians
     * @param pitch (float) - radians
     * @param roll (float) - radians
     * @return state (String)
     */
    private String anglesToState(
            float azimuth, float pitch, float roll,
            float pitch_maximum_abs_value_radians,
            float roll_maximum_abs_value_radians) {
        if ((pitch > 0) && (pitch < pitch_maximum_abs_value_radians)) {
            return FORWARD;
        } else if ((pitch > - pitch_maximum_abs_value_radians) && (pitch < 0)){
            return REVERSE;
        } else if ((roll > 0) && (roll < roll_maximum_abs_value_radians)) {
            return RIGHT;
        } else if ((roll > - roll_maximum_abs_value_radians) && (roll < 0)){
            return LEFT;
        } else {
            return NEUTRAL;
        }
    }


    /**
     * Finish all work with rxJava Observable class.
     * Should be called every time when we stop working with rxjava.
     * (for example activity rotation and others)
     */
    private void deinitializeSensorProcessing() {
        disposable.dispose();
    }

    /**
     * System callback. Will called every time when sensor give us values.
     * Just clone all values to local variables. Should be as fast as possible
     * <p>
     * @param sensorEvent
     * @see SensorEvent
     */
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        switch (sensorEvent.sensor.getType()) {
            case (Sensor.TYPE_ACCELEROMETER): {
                mAccelerometerData = sensorEvent.values.clone();
                break;
            } case (Sensor.TYPE_MAGNETIC_FIELD): {
                mMagnetometerData = sensorEvent.values.clone();
            } default: {
                return;
            }
        }
    }

    /**
     * RotationAnglesCalculator method used for calculating rotation angles
     * by using local arrays mAccelerometerData and mMagnetometerData.
     * Process screen rotation state. Implemented as callable for rxJava.
     * Should be called at Computation Thread.
     * <p>
     * @return      float [] rotation angles - pitch, roll, azimuth
     */
    private static class RotationAnglesCalculator implements Callable<float[]> {

        @Override
        public float[] call() throws Exception {
            float [] rotationMatrix = new float[9];
            boolean rotationMatrixIsOK = SensorManager.getRotationMatrix(
                    rotationMatrix, null, mAccelerometerData, mMagnetometerData);
            float [] rotationAngles = new float[3];
            if (!rotationMatrixIsOK) {
                return rotationAngles;
            }

            float [] rotationMatrixAdjusted = new float[9];
            switch (mDisplay.getOrientation()) {
                case (Surface.ROTATION_0): {
                    rotationMatrixAdjusted = rotationMatrix.clone();
                    break;
                } case (Surface.ROTATION_90): {
                    SensorManager.remapCoordinateSystem(
                            rotationMatrix,
                            SensorManager.AXIS_Y,
                            SensorManager.AXIS_MINUS_X,
                            rotationMatrixAdjusted);
                } case (Surface.ROTATION_180): {
                    SensorManager.remapCoordinateSystem(
                            rotationMatrix,
                            SensorManager.AXIS_MINUS_X,
                            SensorManager.AXIS_MINUS_Y,
                            rotationMatrixAdjusted);
                } case (Surface.ROTATION_270): {
                    SensorManager.remapCoordinateSystem(
                            rotationMatrix,
                            SensorManager.AXIS_MINUS_Y,
                            SensorManager.AXIS_X,
                            rotationMatrixAdjusted);
                } default: {
                    break;
                }
            }
            SensorManager.getOrientation(rotationMatrixAdjusted, rotationAngles);
            return rotationAngles;
        }
    }

    /**
     * This method filter output stream by time. Uses local variables:
     * lastUpdateTimeMsec and updateTargetPeriodMsec
     * <p>
     * @param updateTargetPeriodMsec (long) - update period
     * @return      boolean
     */
    private static boolean timeUpdateFilter(Integer updateTargetPeriodMsec) {
        long actualTimeMsec = System.currentTimeMillis();
        if (actualTimeMsec - lastUpdateTimeMsec < (long)updateTargetPeriodMsec) {
            return false;
        }
        lastUpdateTimeMsec = actualTimeMsec;
        return true;
    }
}
