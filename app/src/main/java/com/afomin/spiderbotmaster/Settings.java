package com.afomin.spiderbotmaster;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class Settings {

    private Settings() {

    }

    /**
     * Extract all parameters from shared preferences file and put them to
     * HashMap
     *
     * @return HashMap with all parameters
     */
    public static Map<String, String> getAllParametersFromSharedPreferences(Context context) {
        Map<String, String> fromSharedPreferences = new HashMap<String, String>();
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                context.getResources().getString(R.string.shared_preferences_file_name),
                MODE_PRIVATE);
        fromSharedPreferences.put(
                context.getResources().getString(R.string.shared_preferences_ip_address_key),
                sharedPreferences.getString(
                        context.getResources().getString(R.string.shared_preferences_ip_address_key),
                        context.getResources().getString(R.string.default_ip_address)));
        fromSharedPreferences.put(
                context.getResources().getString(R.string.shared_preferences_update_target_period_key),
                sharedPreferences.getString(
                        context.getResources().getString(R.string.shared_preferences_update_target_period_key),
                        context.getResources().getString(R.string.default_update_target_period_msec)));
        fromSharedPreferences.put(
                context.getResources().getString(R.string.shared_preferences_target_port_key),
                sharedPreferences.getString(
                        context.getResources().getString(R.string.shared_preferences_target_port_key),
                        context.getResources().getString(R.string.default_target_port)));
        fromSharedPreferences.put(
                context.getResources().getString(R.string.shared_preferences_pitch_neutral_offset_key),
                sharedPreferences.getString(
                        context.getResources().getString(R.string.shared_preferences_pitch_neutral_offset_key),
                        context.getResources().getString(R.string.default_pitch_neutral_offset_angle_degrees)));
        fromSharedPreferences.put(
                context.getResources().getString(R.string.shared_preferences_roll_maximum_abs_value_key),
                sharedPreferences.getString(
                        context.getResources().getString(R.string.shared_preferences_roll_maximum_abs_value_key),
                        context.getResources().getString(R.string.default_roll_maximum_abs_value_angle_degrees)));
        fromSharedPreferences.put(
                context.getResources().getString(R.string.shared_preferences_pitch_maximum_abs_value_key),
                sharedPreferences.getString(
                        context.getResources().getString(R.string.shared_preferences_pitch_maximum_abs_value_key),
                        context.getResources().getString(R.string.default_pitch_maximum_abs_value_angle_degrees)));
        fromSharedPreferences.put(
                context.getResources().getString(R.string.shared_preferences_neutral_position_maximum_abs_value_key),
                sharedPreferences.getString(
                        context.getResources().getString(R.string.shared_preferences_neutral_position_maximum_abs_value_key),
                        context.getResources().getString(R.string.default_neutral_position_maximum_abs_value_degrees)));
        return fromSharedPreferences;
    }

    /**
     * Save all input parameters to shared preferences file.
     * Will not save parameter if it cannot be converted to appropriate format.
     * Appropriate format:
     * String for IP address
     * Int for port
     * Long for period of update
     *
     * @param toSharedPreferences (HashMap with all values)
     */
    public static void saveAllParametersToSharedPreferences(Context context,
                                                            HashMap<String, String> toSharedPreferences) {
        SharedPreferences.Editor sharedPreferencesEdit = context.getSharedPreferences(
                context.getResources().getString(R.string.shared_preferences_file_name),
                MODE_PRIVATE).edit();

        String usersIPAddress = toSharedPreferences.get(context.getResources().getString(R.string.shared_preferences_ip_address_key));
        if (!usersIPAddress.isEmpty()) {
            sharedPreferencesEdit.putString(
                    context.getResources().getString(R.string.shared_preferences_ip_address_key),
                    usersIPAddress);
            sharedPreferencesEdit.commit();
        }
        saveIntToSharedPreferences(
                sharedPreferencesEdit,
                toSharedPreferences.get(context.getResources().getString(R.string.shared_preferences_update_target_period_key)),
                context.getResources().getString(R.string.shared_preferences_update_target_period_key),
                1,
                9999999);
        saveIntToSharedPreferences(
                sharedPreferencesEdit,
                toSharedPreferences.get(context.getResources().getString(R.string.shared_preferences_target_port_key)),
                context.getResources().getString(R.string.shared_preferences_target_port_key),
                1,
                65535);
        saveIntToSharedPreferences(
                sharedPreferencesEdit,
                toSharedPreferences.get(context.getResources().getString(R.string.shared_preferences_pitch_neutral_offset_key)),
                context.getResources().getString(R.string.shared_preferences_pitch_neutral_offset_key),
                context.getResources().getInteger(R.integer.NEUTRAL_OFFSET_VALUE_DEGREES_MINIMUM),
                context.getResources().getInteger(R.integer.NEUTRAL_OFFSET_VALUE_DEGREES_MAXIMUM));
        saveIntToSharedPreferences(
                sharedPreferencesEdit,
                toSharedPreferences.get(context.getResources().getString(R.string.shared_preferences_roll_maximum_abs_value_key)),
                context.getResources().getString(R.string.shared_preferences_roll_maximum_abs_value_key),
                context.getResources().getInteger(R.integer.ROLL_ANGLE_MAXIMUM_ABSOLUTE_VALUE_DEGREES_MINIMUM),
                context.getResources().getInteger(R.integer.ROLL_ANGLE_MAXIMUM_ABSOLUTE_VALUE_DEGREES_MAXIMUM));
        saveIntToSharedPreferences(
                sharedPreferencesEdit,
                toSharedPreferences.get(context.getResources().getString(R.string.shared_preferences_pitch_maximum_abs_value_key)),
                context.getResources().getString(R.string.shared_preferences_pitch_maximum_abs_value_key),
                context.getResources().getInteger(R.integer.PITCH_ANGLE_MAXIMUM_ABSOLUTE_VALUE_DEGREES_MINIMUM),
                context.getResources().getInteger(R.integer.PITCH_ANGLE_MAXIMUM_ABSOLUTE_VALUE_DEGREES_MAXIMUM));

    }

    private static void saveIntToSharedPreferences(
            SharedPreferences.Editor editor,
            String valueToCommit,
            String key,
            Integer minimumValue,
            Integer maximumValue) {
        if (!valueToCommit.isEmpty()) {
            try {
                Integer valueToCommitInt = Integer.parseInt(valueToCommit);
                if ((valueToCommitInt >= minimumValue) && (valueToCommitInt <= maximumValue)) {
                    editor.putString(
                            key,
                            valueToCommit);
                    editor.commit();
                }
            } catch (NumberFormatException ex) {
            }
        }
    }
}

