package com.afomin.spiderbotmaster;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.HashMap;
import java.util.Map;

import androidx.fragment.app.DialogFragment;


/**
 * A simple {@link DialogFragment} subclass.

 */
public class SettingsDialogFragment extends DialogFragment implements View.OnClickListener {

    private final String TAG = "SpiderBotMasterDebug";
    private EditText editTextIPAddress;
    private EditText editTextPort;
    private EditText editTextPeriodMSEC;
    private EditText editTextPitchNeutralOffsetDegrees;
    private EditText editTextRollMaximumAbs;
    private EditText editTextPitchMaximumAbs;
    private EditText editTextNeutralPositionMaximumAbs;
    private Button buttonSave;
    private Map<String, String> mapSharedPreferences;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_settings_dialog, null);
        getDialog().setTitle(getResources().getString(R.string.settings_dialog_title));
        // setup UI
        buttonSave = v.findViewById(R.id.button_save);
        buttonSave.setOnClickListener(this);
        editTextIPAddress = v.findViewById(R.id.edit_text_ip_address);
        editTextPort = v.findViewById(R.id.edit_text_port);
        editTextPeriodMSEC = v.findViewById(R.id.edit_text_period_msec);
        editTextPitchNeutralOffsetDegrees = v.findViewById(R.id.edit_text_pitch_neutral_offset_degrees);
        editTextRollMaximumAbs = v.findViewById(R.id.edit_text_roll_angle_maximum_abs);
        editTextPitchMaximumAbs = v.findViewById(R.id.edit_text_pitch_angle_maximum_abs);
        editTextNeutralPositionMaximumAbs = v.findViewById(R.id.edit_text_neutral_position_maximum_abs);

        // get all saved parameters
        mapSharedPreferences  = Settings.getAllParametersFromSharedPreferences(getActivity());

        // set hints
        editTextIPAddress.setHint(mapSharedPreferences.get(
                getResources().getString(R.string.shared_preferences_ip_address_key)));
        editTextPort.setHint(mapSharedPreferences.get(
                getResources().getString(R.string.shared_preferences_target_port_key)));
        editTextPeriodMSEC.setHint(mapSharedPreferences.get(
                getResources().getString(R.string.shared_preferences_update_target_period_key)));
        editTextPitchNeutralOffsetDegrees.setHint(mapSharedPreferences.get(
                getResources().getString(R.string.shared_preferences_pitch_neutral_offset_key)));
        editTextRollMaximumAbs.setHint(mapSharedPreferences.get(
                getResources().getString(R.string.shared_preferences_roll_maximum_abs_value_key)));
        editTextPitchMaximumAbs.setHint(mapSharedPreferences.get(
                getResources().getString(R.string.shared_preferences_pitch_maximum_abs_value_key)));
        editTextNeutralPositionMaximumAbs.setHint(mapSharedPreferences.get(
                getResources().getString(R.string.shared_preferences_neutral_position_maximum_abs_value_key)));
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_save: {
                HashMap<String, String> toSharedPreferences = new HashMap<String, String>();
                toSharedPreferences.put(getResources().getString(R.string.shared_preferences_ip_address_key),
                        editTextIPAddress.getText().toString());
                toSharedPreferences.put(getResources().getString(R.string.shared_preferences_target_port_key),
                        editTextPort.getText().toString());
                toSharedPreferences.put(getResources().getString(R.string.shared_preferences_update_target_period_key),
                        editTextPeriodMSEC.getText().toString());
                toSharedPreferences.put(getResources().getString(R.string.shared_preferences_pitch_neutral_offset_key),
                        editTextPitchNeutralOffsetDegrees.getText().toString());
                toSharedPreferences.put(getResources().getString(R.string.shared_preferences_roll_maximum_abs_value_key),
                        editTextRollMaximumAbs.getText().toString());
                toSharedPreferences.put(getResources().getString(R.string.shared_preferences_pitch_maximum_abs_value_key),
                        editTextPitchMaximumAbs.getText().toString());
                toSharedPreferences.put(getResources().getString(R.string.shared_preferences_neutral_position_maximum_abs_value_key),
                        editTextNeutralPositionMaximumAbs.getText().toString());
                Settings.saveAllParametersToSharedPreferences(
                        getActivity(),
                        toSharedPreferences);
                dismiss();
                break;
            }
            default: {

            }
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        ((MainActivity)getActivity()).restartCommunication();
    }
}
